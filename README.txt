REQUIRES

python 3
pyqt5
qdarkstyle


TOOLS

MediaInfo.dll
x264.exe (rev2431 tMod) 2014-04-24
ffmpeg.exe (git-0c90b2e) (2016-04-09)
neroAacEnc.exe (1.5.1.0) 2009-12-17
mkvmerge.exe (9.0.1) 2016-03-28


Changelog

1.0 2016-04-10

Updated tools to current versions
Fixed bug where the preceding zeroes are dropped for the generated CRC
Fixed Windows taskbar icon not displaying properly
Fixed Windows application file icon not displaying properly