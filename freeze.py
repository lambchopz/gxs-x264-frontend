import sys
from cx_Freeze import setup, Executable

build_exe_options = {"include_files": ["tools", "icons"]}

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
	name = "GXS x264 Frontend",
	version = "1.0",
	author = "Alvin Lam",
	options = {"build_exe": build_exe_options},
	description = "x264, ffmpeg, neroAacEnc, mkvmerge frontend",
	executables = [Executable("GXSx264Frontend.py", base = base, icon="gxs-256.ico")])

#python freeze.py build